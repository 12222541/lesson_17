// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <cmath>
#include <iostream>



class Vector
{
private:
    int x;
    int y;
    int z;

public:
    Vector() : x(0), y(0), z(0)  // инициализация
    {}
    Vector(int _x, int _y, int _z) : x(_x), y(_y), z(_z) // конструктор
    {}

    int Get (char A)
    { 
        if (A == 'x')
        {
            return x;
        }
        else if (A == 'y')
        {
            return y;
        }
        else if (A == 'z')
        {
            return z;
        }
    }

    void Set (int x1, int y1, int z1)
    {
        x = x1;
        y = y1;
        z = z1;
    }
    
    void Length ()
    {
        std::cout << sqrt((x * x) + y * y + z * z) << '\n';
    }
};


int main()
{
    Vector V (1, 2, 9);
    V.Set (2, 3, 6);
    std::cout << V.Get ('x') << '\n';
    std::cout << V.Get('y') << '\n';
    std::cout << V.Get('z') << '\n';
    V.Length();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
